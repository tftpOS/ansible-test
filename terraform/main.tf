provider "aws" {
  region = "eu-central-1"
}

data "aws_ami" "ubuntu_latest" {
  owners      = ["099720109477"]
  most_recent = true # Означает самый последний
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_instance" "my_Ubuntu_01" {
  ami                    = data.aws_ami.ubuntu_latest.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_web_server.id]
  key_name               = aws_key_pair.example.key_name

  tags = {
    Name    = "Any Ubuntu"
    Owner   = "TFTP"
    Project = "AWS Learning"
  }
}

resource "aws_instance" "my_Ubuntu_02" {
  ami                    = data.aws_ami.ubuntu_latest.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_web_server.id]
  key_name               = aws_key_pair.example.key_name

  tags = {
    Name    = "Any Ubuntu"
    Owner   = "TFTP"
    Project = "AWS Learning"
  }
}

resource "aws_key_pair" "example" {
  key_name   = "id_rsa_key"
  public_key = file("~/.ssh/id_rsa.pub")
}


resource "aws_security_group" "my_web_server" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    for_each = ["80", "22"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Dynamic Security Group"
    Owner = "TFTP"
  }
}

output "ubuntu_server_1_ip" {
  value = aws_instance.my_Ubuntu_01.public_ip
}

output "ubuntu_server_2_ip" {
  value = aws_instance.my_Ubuntu_02.public_ip
}

output "key_pairs_name" {
  value = aws_key_pair.example.key_name
}
